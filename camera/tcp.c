 #include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#include "tcp.h"



int socketInit(void)
{
	int sockid;
    sockid = socket(AF_INET, SOCK_STREAM, 0);
    if(sockid < 0)
    {
        perror("sockid error");
        return -1;
    }
    printf("sockid ok\r\n");

    int on = 1;
    setsockopt(sockid,SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SPORT);
    addr.sin_addr.s_addr = INADDR_ANY;
    int ret = bind(sockid, (struct sockaddr*)&addr,sizeof(addr));
    if(ret < 0)
    {
        perror("bind error");
        close(sockid);
        return -1;
    }
    printf("bind ok\r\n");

    ret = listen(sockid, 10);
    if(ret < 0)
    {
        perror("listen error");
        close(sockid);
        return -1;
    }
    printf("listening...\r\n");
    return sockid;
}
