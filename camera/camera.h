#ifndef _CAMERA_H_
#define _CAMERA_H_

#include <linux/videodev2.h>

#define IMAGEHEIGHT 480
#define IMAGEWIDTH 640
#define FRAME_NUM 5
#define NAME "JPEG"
#define DEFAULT_DEVICE "/dev/video0"  
#define SIZE 1024

struct VideoBuffer //定义一个结构体指针来映射每个缓冲帧
{   
    void *start;
    size_t length;
};

//设置视频格式
int set_VideoType(int cameraFd);

//申请帧缓冲 ，分配视频缓冲区
int applyfor_VideoBuffer(int cameraFd);

//查询分配的视频缓冲区
int serach_VideoBuffer(int cameraFd);

//将20个缓冲帧放入队列中并启动数据流
int set_BufferToQueue(int cameraFd);

//获取一帧图像
char * get_Piture(int cameraFd, struct v4l2_buffer * pbuf);

//将yuyv格式转换为RGB格式
void yuyv_to_rgb(unsigned char* yuv,unsigned char* rgb);


//将RGB砖转换为JPG格式
unsigned long rgb_to_jpeg(char *rgb, unsigned char **jpeg);

//出队函数
int dequeue(int cameraFd, unsigned int *index);

//入队函数
int enqueue(int cameraFd, unsigned int index);

#endif
