#include <stdio.h> 
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <pthread.h> 
#include <unistd.h>          //close
#include <sys/ioctl.h>       //ioctl
#include <stdlib.h>          //EXIT_FAILURE
#include <linux/videodev2.h> //V4L2
#include <string.h>          //memset
#include <sys/mman.h> 
#include <sys/select.h>
#include <sys/time.h>
#include <errno.h>
#include "jconfig.h"
#include "jerror.h"
#include "jmorecfg.h"
#include "jpeglib.h"	   
#include "camera.h"

//struct VideoBuffer buffers[FRAME_NUM];
struct VideoBuffer *buffers = NULL;
//设置视频格式
int set_VideoType(int cameraFd)
{
	struct v4l2_format fmt;
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if(ioctl(cameraFd, VIDIOC_G_FMT, &fmt) < 0)
	{
		perror("VIDIOC_G_FMT");
		return -1;
	}
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;//yuv格式
	fmt.fmt.pix.height = IMAGEHEIGHT;
	fmt.fmt.pix.width = IMAGEWIDTH;
	fmt.fmt.pix.field = V4L2_FIELD_INTERLACED;

	int ret = ioctl(cameraFd, VIDIOC_S_FMT, &fmt);
	if(0 > ret)
	{
		printf("set fmt error\r\n");
		return -1;
	}
	return 0;
}

//申请帧缓冲 ，分配视频缓冲区
int applyfor_VideoBuffer(int cameraFd)
{
	struct v4l2_requestbuffers  req;
	unsigned int n_buffers;

	memset(&req, 0, sizeof(struct v4l2_requestbuffers));
	req.count = FRAME_NUM;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	int ret = ioctl(cameraFd, VIDIOC_REQBUFS, &req);
	if(0 > ret)
	{
		printf("request buffers error\r\n");
		return -1;
	}
	return 0;
}

//查询分配的视频缓冲区
int serach_VideoBuffer(int cameraFd)
{
	struct v4l2_buffer buf;
	unsigned int n_buffers;
	memset(&buf, 0, sizeof(buf));
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buffers = (struct VideoBuffer *)malloc(FRAME_NUM * sizeof(struct VideoBuffer));
	memset(buffers, 0, FRAME_NUM * sizeof(struct VideoBuffer));
	for(n_buffers = 0; n_buffers < FRAME_NUM; n_buffers++) //映射所有的缓存
	{	
		buf.index = n_buffers;
		if(ioctl(cameraFd, VIDIOC_QUERYBUF, &buf) == -1)
		{//获取到对应index的缓存信息，此处主要利用length信息及offset信息来完成后面的mmap操作
			printf("query buffer error\n");
			return -1;
		}
		buffers[n_buffers].length = buf.length;
		
		buffers[n_buffers].start = (char *)mmap(NULL, buf.length, 
						PROT_READ|PROT_WRITE,
						MAP_SHARED, 
						cameraFd,
						buf.m.offset);
		if(buffers[n_buffers].start == MAP_FAILED)
		{
			perror("map error");
			printf("buffer map error\n");
			return -1;
		}
	}
	return 0;
	
}

//将20个缓冲帧放入队列中并启动数据流
int set_BufferToQueue(int cameraFd)
{
	struct v4l2_buffer buf;
	unsigned int n_buffers;
	int ret = 0;
	memset(&buf, 0, sizeof(buf));
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	for(n_buffers = 0; n_buffers < FRAME_NUM; n_buffers++)
	{
		buf.index = n_buffers;
		/* 将空闲的内存加入可捕获视频的队列 */
		if(ioctl(cameraFd, VIDIOC_QBUF, &buf)<0)
		{
			 perror("ERROR: VIDIOC_STREAMON");
			 return -1;
		}
	}
	//启动串流
	//int type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	//ioctl(cameraFd, VIDIOC_STREAMON, &type); 
	return 0;
}


//yuyv转rgb
void yuyv_to_rgb(unsigned char* yuv,unsigned char* rgb)
{
    unsigned int i;
    unsigned char* y0 = yuv + 0;   
    unsigned char* u0 = yuv + 1;
    unsigned char* y1 = yuv + 2;
    unsigned char* v0 = yuv + 3;

    unsigned  char* r0 = rgb + 0;
    unsigned  char* g0 = rgb + 1;
    unsigned  char* b0 = rgb + 2;
    unsigned  char* r1 = rgb + 3;
    unsigned  char* g1 = rgb + 4;
    unsigned  char* b1 = rgb + 5;
   
    float rt0 = 0, gt0 = 0, bt0 = 0, rt1 = 0, gt1 = 0, bt1 = 0;

    for(i = 0; i <= (IMAGEWIDTH * IMAGEHEIGHT) / 2 ;i++)
    {
        bt0 = 1.164 * (*y0 - 16) + 2.018 * (*u0 - 128); 
        gt0 = 1.164 * (*y0 - 16) - 0.813 * (*v0 - 128) - 0.394 * (*u0 - 128); 
        rt0 = 1.164 * (*y0 - 16) + 1.596 * (*v0 - 128); 
   
    	bt1 = 1.164 * (*y1 - 16) + 2.018 * (*u0 - 128); 
        gt1 = 1.164 * (*y1 - 16) - 0.813 * (*v0 - 128) - 0.394 * (*u0 - 128); 
        rt1 = 1.164 * (*y1 - 16) + 1.596 * (*v0 - 128); 
    
      
       	if(rt0 > 250)  	rt0 = 255;
		if(rt0< 0)    	rt0 = 0;	

		if(gt0 > 250) 	gt0 = 255;
		if(gt0 < 0)	gt0 = 0;	

		if(bt0 > 250)	bt0 = 255;
		if(bt0 < 0)	bt0 = 0;	

		if(rt1 > 250)	rt1 = 255;
		if(rt1 < 0)	rt1 = 0;	

		if(gt1 > 250)	gt1 = 255;
		if(gt1 < 0)	gt1 = 0;	

		if(bt1 > 250)	bt1 = 255;
		if(bt1 < 0)	bt1 = 0;	
					
		*r0 = (unsigned char)rt0;
		*g0 = (unsigned char)gt0;
		*b0 = (unsigned char)bt0;
	
		*r1 = (unsigned char)rt1;
		*g1 = (unsigned char)gt1;
		*b1 = (unsigned char)bt1;

        yuv = yuv + 4;
        rgb = rgb + 6;
        if(yuv == NULL)
          break;

        y0 = yuv;
        u0 = yuv + 1;
        y1 = yuv + 2;
        v0 = yuv + 3;
  
        r0 = rgb + 0;
        g0 = rgb + 1;
        b0 = rgb + 2;
        r1 = rgb + 3;
        g1 = rgb + 4;
        b1 = rgb + 5;
    }   
}


//RGB压缩为JPEG
unsigned long rgb_to_jpeg(char *rgb, unsigned char **jpeg)
{
	unsigned long jpeg_size;
	struct jpeg_compress_struct jcs;
	struct jpeg_error_mgr jem;
	JSAMPROW row_pointer[1];
	int row_stride;
	
	jcs.err = jpeg_std_error(&jem);
	jpeg_create_compress(&jcs);
	
	jpeg_mem_dest(&jcs, jpeg, &jpeg_size);//就是这个函数！！！！！！！
	
	jcs.image_width = IMAGEWIDTH;
	jcs.image_height = IMAGEHEIGHT;

	jcs.input_components = 3;//1;
	jcs.in_color_space = JCS_RGB;//JCS_GRAYSCALE;

	jpeg_set_defaults(&jcs);
	jpeg_set_quality(&jcs, 40, TRUE);
	
	jpeg_start_compress(&jcs, TRUE);
	row_stride =jcs.image_width * 3;
	
	while(jcs.next_scanline < jcs.image_height){//对每一行进行压缩
		row_pointer[0] = &rgb[jcs.next_scanline * row_stride];
		(void)jpeg_write_scanlines(&jcs, row_pointer, 1);
	}
	jpeg_finish_compress(&jcs);
	jpeg_destroy_compress(&jcs);

	return jpeg_size;
}
//出队函数
int dequeue(int cameraFd, unsigned int *index)
{
	int ret;
	fd_set fd;
	struct timeval timeout;
	struct v4l2_buffer buf;
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	FD_ZERO(&fd);
	FD_SET(cameraFd,&fd);
	timeout.tv_sec = 5;
	timeout.tv_usec = 0;
	
	ret = select(cameraFd+1,&fd,NULL,NULL,&timeout);
	if(-1 == ret)
	{
		perror("select error:"); 
		if (errno == EINTR)
			return -1;
		else
			return -1;
	}
	else if(0 == ret)
	{
		fprintf(stderr, "camera->dqbuf: dequeue buffer timeout\n");
		return -1;
	}
	else
	{
		ret = ioctl(cameraFd,VIDIOC_DQBUF,&buf);
		if(-1 == ret)
		{
			perror("dequeue error:");
			return -1;
		}
		*index = buf.index;
		return 0;
	}	
}


//入队函数
int enqueue(int cameraFd, unsigned int index)
{
	int ret= 0;
	struct v4l2_buffer buf;
	buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	buf.index = index;
	ret = ioctl(cameraFd,VIDIOC_QBUF, &buf);
	if(ret < 0)
	{
		perror("enqueue()->QBUFerror:");
		return -1;
	}
	return 0;
}
