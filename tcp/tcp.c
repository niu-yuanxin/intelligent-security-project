/*===============================================================
 *   Copyright (C) 2020 All rights reserved.
 *   
 *   文件名称：tcp.c
 *   创 建 者：刘世豪
 *   创建日期：2020年10月28日
 *   描    述：
 *
 *   更新日志：
 *
 ================================================================*/
#include "tcp_port.h"
#define IP "192.168.0.225"
#define SPORT 8888
#define SIZE 1024

int main(int argc, const char *argv[])
{
    int sockid = 0;
    sockid = socketInit(IP,SPORT);

    int connid = accept(sockid, NULL, NULL);
    if(connid < 0)
    {
        perror("connid error");
        close(sockid);
        return -1;
    }
    printf("connid ok\r\n");

    char fileHead[50]={0};
    char *p = get_FileHead("./1.png");
    strcpy(fileHead,p);
    free(p);

    send( connid, fileHead, sizeof(fileHead), 0 );
    int fd = open("./1.png",O_RDONLY);
    if(fd < 0)
    {
        perror("open error");
        close(connid);
        close(sockid);
        return -1;
    }


    sendFile("./1.png",sockid, connid, fd);


    return 0;
}
