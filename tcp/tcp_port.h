/*===============================================================
*   Copyright (C) 2020 All rights reserved.
*   
*   文件名称：tcp_port.h
*   创 建 者：刘世豪
*   创建日期：2020年10月30日
*   描    述：
*
*   更新日志：
*
================================================================*/
#ifndef _TCP_PORT_H
#define _TCP_PORT_H
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/epoll.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <strings.h>
#define SIZE 1024

/*
 *函数功能：tcp服务器初始化
 *函数名：socketInit
 *函数参数：IP port
 *函数返回值：sockid
 */
int socketInit( const char *IP, int SPORT);

/*
 *函数名：get_FileHead
 *函数功能：获取文件头
 *函数参数：
        filePath  --- 文件路径
 *函数返回值：
        fileHead  --- 文件头
 */
char * get_FileHead(char * filePath);
    


/*
 *函数名:sendFile
 *函数功能：发送文件给客户端
 *函数参数：
        filePath --- 文件路径
        sockid --- 套接字描述符
        connid --- 
 *函数返回值:
        成功返回0，失败返回-1
 */
void sendFile(char * filePath, int sockid, int connid, int fd);
#endif
