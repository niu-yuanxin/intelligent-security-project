/*===============================================================
 *   Copyright (C) 2020 All rights reserved.
 *   
 *   文件名称：tcp_port.c
 *   创 建 者：刘世豪
 *   创建日期：2020年10月30日
 *   描    述：
 *
 *   更新日志：
 *
 ================================================================*/
#include "tcp_port.h"

int socketInit(const char *IP,int SPORT)
{
    int sockid;
    sockid = socket(AF_INET, SOCK_STREAM, 0);
    if(sockid < 0)
    {
        perror("sockid error");
        return -1;
    }
    printf("sockid ok\r\n");

    int on = 1;
    setsockopt(sockid,SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on));

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(SPORT);
    addr.sin_addr.s_addr = INADDR_ANY;
    int ret = bind(sockid, (struct sockaddr*)&addr,sizeof(addr));
    if(ret < 0)
    {
        perror("bind error");
        close(sockid);
        return -1;
    }
    printf("bind ok\r\n");

    ret = listen(sockid, 10);
    if(ret < 0)
    {
        perror("listen error");
        close(sockid);
        return -1;
    }
    printf("listen ok\r\n");
    return sockid;
}


char * get_FileHead(char * filePath)
{

    int fileSize;
    char *pfilehead = (char *)malloc(sizeof(char)*50);
    char size[20] = {0};
    struct stat stBuf = {0};
    stat( filePath,&stBuf );
    fileSize = stBuf.st_size;
    strcpy( pfilehead, filePath);
    strcat( pfilehead, "##" );
    sprintf( size, "%d", fileSize );
    strcat( pfilehead, size );
    return pfilehead;
}


void sendFile(char * filePath, int sockid, int connid, int fd)
{
    char buf[SIZE] = {0};
    while(1)
    {
        memset(buf, 0, sizeof(buf));
        int ret = read(fd,buf,sizeof(buf));
        if(ret == 0)
        {
            printf("read over\r\n");
            close(sockid);
            close(connid);
            break;
        }
        ret = send(connid,buf,sizeof(buf),0);
        if(ret < 0)
        {
            perror("recv error");
            close(connid);
            close(sockid);
            return ;
        }
    }
}
