#include "widget.h"
#include "ui_widget.h"
#include <QTime>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    connect(&socket,SIGNAL(connected()),this,SLOT(connectedSlot()));
    connect(&socket,SIGNAL(disconnected()),this,SLOT(disconnectedSlot()));
    connect(&socket,SIGNAL(readyRead()),this,SLOT(recvDataSlot()));
    flag = true;
    i = 0;

}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_connectBtn_clicked()
{
    //连接服务器
    socket.connectToHost("192.168.0.155", 9999);
    //如果连接成功就会有connected的信号产生
}

void Widget::connectedSlot()
{
    ui->connectBtn->setEnabled(false);
}

void Widget::disconnectedSlot()
{
    ui->connectBtn->setEnabled(true);
}


void Widget::recvDataSlot()
{

    //收文件头
    if(true == flag)
    {
        char * buf = (char *)malloc(sizeof(char)*50);
        memset(buf, 0, 50);
        socket.read(buf, sizeof(char)*50);
        QString str = QString(buf);
        //cqDebug() <<"buf: "<< buf;
        fileName = str.section("##", 0, 0);
        fileSize = str.section("##", 1, 1).toInt();
        flag = false;
        free(buf);
    }
    else
    {
        //收文件内容
        arr += socket.read(1024);
        if(arr.size() >= fileSize)
        {
            flag = true;
            QPixmap pix;
            bool ok = pix.loadFromData(arr);
            if(true == ok)
            {
                ui->label->setPixmap(pix);
                ui->label->setScaledContents(true);
                arr = {0};
                qDebug()<< "readover "<< i;
                i++;
            }
        }
    }
}

